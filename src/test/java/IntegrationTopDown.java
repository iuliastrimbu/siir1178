import evaluator.controller.AppController;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegrationTopDown {
    private IntrebariRepository ir = new IntrebariRepository();
    private AppController appctrl = new AppController();



    @org.junit.Test
    public void TCF01()throws Exception {
        //verif daca a adaugat o pentru domeniul respectiv
        ir.addIntrebareNoua("Intrebare1?", "1)A","2)B", "1", "Domeniu1");
        ir.addIntrebareNoua("Intrebare2?", "1)A","2)B", "1", "Domeniu2");
        ir.addIntrebareNoua("Intrebare3?", "1)A","2)B", "1", "Domeniu3");
        ir.addIntrebareNoua("Intrebare4?", "1)A","2)B", "1", "Domeniu4");
        assertEquals(1,ir.getIntrebariByDomain("Domeniu1").size());
        ir.addIntrebareNoua("Intrebare5?", "1)A","2)B", "1", "Domeniu1");
        assertEquals(2,ir.getIntrebariByDomain("Domeniu1").size());
    }

    //creeaza test

    @org.junit.Test
    public void TCF02()throws Exception {
        Intrebare i1 = new Intrebare ("Intrebare1?", "1)A","2)B", "1", "Domeniu1");
        Intrebare i2 = new Intrebare ("Intrebare2?", "1)A","2)B", "1", "Domeniu2");
        Intrebare i3 = new Intrebare ("Intrebare3?", "1)A","2)B", "1", "Domeniu3");
        Intrebare i4 = new Intrebare ("Intrebare4?", "1)A","2)B", "1", "Domeniu4");
        Intrebare i5 = new Intrebare ("Intrebare5?", "1)A","2)B", "1", "Domeniu5");
        Intrebare i6 = new Intrebare ("Intrebare6?", "1)A","2)B", "1", "Domeniu1");
        Intrebare i7 = new Intrebare ("Intrebare7?", "1)A","2)B", "1", "Domeniu2");
        Intrebare i8 = new Intrebare ("Intrebare8?", "1)A","2)B", "1", "Domeniu3");
        Intrebare i9 = new Intrebare ("Intrebare9?", "1)A","2)B", "1", "Domeniu4");
        Intrebare i10 = new Intrebare ("Intrebare10?", "1)A","2)B", "1", "Domeniu5");
        appctrl.addNewIntrebare(i1); appctrl.addNewIntrebare(i2); appctrl.addNewIntrebare(i3); appctrl.addNewIntrebare(i4); appctrl.addNewIntrebare(i5);
        appctrl.addNewIntrebare(i6); appctrl.addNewIntrebare(i7); appctrl.addNewIntrebare(i8); appctrl.addNewIntrebare(i9); appctrl.addNewIntrebare(i10);

        Test test = appctrl.createNewTest();
        int size = test.getIntrebari().size();
        //System.out.println("Size test:" + size);
        assertEquals(5, size);
    }

    //f03-e adaugata la domeniu in statistica
    @org.junit.Test
    public void TCF03()throws Exception {

        Intrebare i1 = new Intrebare ("Intrebare1?", "1)A","2)B", "1", "Domeniu1");
        Intrebare i2 = new Intrebare ("Intrebare2?", "1)A","2)B", "1", "Domeniu2");
        Intrebare i3 = new Intrebare ("Intrebare3?", "1)A","2)B", "1", "Domeniu3");
        Intrebare i4 = new Intrebare ("Intrebare4?", "1)A","2)B", "1", "Domeniu4");
        Intrebare i5 = new Intrebare ("Intrebare5?", "1)A","2)B", "1", "Domeniu5");
        Intrebare i6 = new Intrebare ("Intrebare6?", "1)A","2)B", "1", "Domeniu1");
        appctrl.addNewIntrebare(i1); appctrl.addNewIntrebare(i2); appctrl.addNewIntrebare(i3); appctrl.addNewIntrebare(i4); appctrl.addNewIntrebare(i5);  appctrl.addNewIntrebare(i6);

        Statistica statistica = appctrl.getStatistica();
        System.out.println(statistica.toString());
        //System.out.println("f03");
        //System.out.println(statistica.getIntrebariDomenii().get("Domeniu1"));
        //assertEquals(2, statistica.getIntrebariDomenii().size());
        assertEquals(2, statistica.getIntrebariDomenii().get("Domeniu1").intValue());
    }

    //p-a
    @org.junit.Test
    public void TCPA()throws Exception {
        Intrebare i1 = new Intrebare("Intrebare1?", "1)A", "2)B", "1", "Domeniu1");
        appctrl.addNewIntrebare(i1);
        assertTrue(appctrl.exists(i1));
    }
     //p-a-b
     @org.junit.Test
     public void TCPAB()throws Exception {
         Intrebare i1 = new Intrebare("Intrebare1?", "1)A", "2)B", "1", "Domeniu1");
         Intrebare i2 = new Intrebare("Intrebare2?", "1)A", "2)B", "1", "Domeniu2");
         Intrebare i3 = new Intrebare("Intrebare3?", "1)A", "2)B", "1", "Domeniu3");
         Intrebare i4 = new Intrebare("Intrebare4?", "1)A", "2)B", "1", "Domeniu4");
         Intrebare i5 = new Intrebare("Intrebare5?", "1)A", "2)B", "1", "Domeniu5");
         appctrl.addNewIntrebare(i1);
         appctrl.addNewIntrebare(i2);
         appctrl.addNewIntrebare(i3);
         appctrl.addNewIntrebare(i4);
         appctrl.addNewIntrebare(i5);
         Test test = appctrl.createNewTest();
         assertEquals(5, test.getIntrebari().size());

     }
     //p-a-b-c
         @org.junit.Test
         public void TCPABC()throws Exception {
             Intrebare i1 = new Intrebare ("Intrebare1?", "1)A","2)B", "1", "Domeniu1");
             Intrebare i2 = new Intrebare ("Intrebare2?", "1)A","2)B", "1", "Domeniu2");
             Intrebare i3 = new Intrebare ("Intrebare3?", "1)A","2)B", "1", "Domeniu3");
             Intrebare i4 = new Intrebare ("Intrebare4?", "1)A","2)B", "1", "Domeniu4");
             Intrebare i5 = new Intrebare ("Intrebare5?", "1)A","2)B", "1", "Domeniu5");
             appctrl.addNewIntrebare(i1); appctrl.addNewIntrebare(i2); appctrl.addNewIntrebare(i3); appctrl.addNewIntrebare(i4); appctrl.addNewIntrebare(i5);
             Test test = appctrl.createNewTest();
             Statistica statistica = appctrl.getStatistica();
             assertEquals(1, statistica.getIntrebariDomenii().get("Domeniu3").intValue());

    }
}
